﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float travelDistance;

    [SerializeField]
    private float bulletSpeed = 4f;

    private bool isMoving;
    private Vector3 startPos;

	// Update is called once per frame
	void Update () {
		if (isMoving)
        {
            if (Vector2.Distance(startPos, transform.position) < travelDistance)
            {
                transform.position +=  transform.up * bulletSpeed * Time.deltaTime;
            }
           
            else
            {
                DeactivateBullet(true);
            }
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Tower"))
        {
            DeactivateBullet(false);
            collision.gameObject.GetComponent<Tower>().DeactivateTower();
        }
    }

    public void ActivateBullet(float distance)
    {

        startPos = transform.position;

        travelDistance = distance;
        gameObject.SetActive(true);
        isMoving = true;
    }

    private void DeactivateBullet(bool spawnTower)
    {
        isMoving = false;
        gameObject.SetActive(false);
        if (spawnTower)
        {
            Spawner.instance.SpawnTower(transform.position, true);
        }
    }
}
