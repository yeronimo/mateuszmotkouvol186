﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    private Vector3 randomRot;
    private bool isRotating;
    private ObjectPool bulletPool;
    private GameObject bullet;
    private int shotsCounter = 0;

    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private Transform bulletSpawn;
    [SerializeField]
    private float timeBetweenRotate = .5f;
    [SerializeField]
    private int shotAmount = 1;
    [SerializeField]
    private Color activeColor;
    [SerializeField]
    private Color inActiveColor;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Use this for initialization
    void Start ()
    {

        bulletPool = ObjectPoolManager.instance.GetPoolByName("bullets");      
    }

   
    private IEnumerator RandomRotateAndShoot(float delay)
    {
        yield return new WaitForSeconds(delay);
        SetTowerColor(activeColor);
        while (shotsCounter < shotAmount)
        {
            yield return new WaitForSeconds(timeBetweenRotate);
            transform.Rotate(Vector3.forward, Random.Range(15f, 45f));
            Shot();
        }
        StopShooting();
    }

    private void Shot()
    {
        bullet = ObjectPoolManager.instance.GetObjectFromPool(bulletPool);
        bullet.transform.position = bulletSpawn.position;
        bullet.transform.rotation = transform.rotation;
        bullet.GetComponent<Bullet>().ActivateBullet(Random.Range(1,4));

        // bullet = Instantiate(bulletPrfab, bulletSpawn.position, transform.rotation);
        // bullet.GetComponent<Bullet>().ActivateBullet(bulletSpawn.position, 5f);

        shotsCounter++;
    }
    private void SetTowerColor(Color color)
    {
        spriteRenderer.color = color;
    }


    public void ActivateTower()
    {
        gameObject.SetActive(true);
        StartCoroutine(RandomRotateAndShoot(0f));
    }

    public void ActivateTowerDelayed()
    {
        gameObject.SetActive(true);
        StartCoroutine(RandomRotateAndShoot(6f));
    }

    private void StopShooting()
    {
        SetTowerColor(inActiveColor);
        StopCoroutine(RandomRotateAndShoot(0f));
    }

    public void DeactivateTower()
    {
        StopShooting();
        gameObject.SetActive(false);
    }
}
