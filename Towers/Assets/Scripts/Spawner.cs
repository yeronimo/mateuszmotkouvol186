﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour {

    public static Spawner instance;
    public static bool createNewTower = true;

    [SerializeField]
    private int maxTowersQuantity = 10;
    [SerializeField]
    private Text towerCounterTxt;
    private int towerCounter = 0;

    private ObjectPool towerPool;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        createNewTower = true;
        towerPool = ObjectPoolManager.instance.GetPoolByName("towers");
        SetTowerCounter();
        SpawnTower(transform.position, false);
    }

    public void SpawnTower(Vector3 pos, bool delayed)
    {
        if (towerCounter == maxTowersQuantity)
        {
            createNewTower = false;
            foreach (GameObject tower in towerPool.ObjectsPool)
            {
                if (tower.activeSelf)
                {
                    tower.GetComponent<Tower>().ActivateTower();
                }
            }
        }

        if (createNewTower)
        {
            GameObject tower = ObjectPoolManager.instance.GetObjectFromPool(towerPool);
            tower.transform.position = pos;
            tower.transform.rotation = Quaternion.identity;
            if (delayed)
                tower.GetComponent<Tower>().ActivateTowerDelayed();
            else
                tower.GetComponent<Tower>().ActivateTower();
            SetTowerCounter();
        }
    }
	
    public void SetTowerCounter()
    {
        towerCounter++;
        towerCounterTxt.text = "Towers: " + towerCounter.ToString();

    }

}
