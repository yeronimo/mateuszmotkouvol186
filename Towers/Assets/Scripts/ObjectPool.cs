﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPool
{
    public string poolName;
    public Transform parentForObjects;
    public GameObject objectPrefab;
    public List<GameObject> ObjectsPool;
    public int objectCountInit = 0;

    public ObjectPool(string poolName, Transform parentForObjects, GameObject objectPrefabs, List<GameObject> objectsPool, int objectCountInit)
    {
        this.poolName = poolName;
        this.parentForObjects = parentForObjects;
        this.objectPrefab = objectPrefabs;
        ObjectsPool = objectsPool;
        this.objectCountInit = objectCountInit;
    }
}
