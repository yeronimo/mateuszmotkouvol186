﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour
{
    public static ObjectPoolManager instance;

    [SerializeField]
    private List<ObjectPool> objectPoolList;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        InitObjectPoolList();
    }

    private void InitObjectPoolList()
    {
        foreach (ObjectPool objectPool in objectPoolList)
        {
            GameObject parent = new GameObject(objectPool.poolName);
            parent.transform.parent = transform;
            objectPool.parentForObjects = parent.transform;
            if (objectPool.objectCountInit > 0)
                AddObjectsToPool(objectPool);
        }
    }

    public void AddObjectsToPool(ObjectPool pool)
    {
        for (int i = 0; i < pool.objectCountInit; i++)
        {
            AddObjectToPool(pool);
        }
    }

    private GameObject AddObjectToPool(ObjectPool pool)
    {
        GameObject goToPool = Instantiate(pool.objectPrefab);
        goToPool.transform.parent = pool.parentForObjects;
        pool.ObjectsPool.Add(goToPool);
        goToPool.SetActive(false);
        return goToPool;
    }

    public GameObject InstantiateObjectPrefabToPool(string poolName, GameObject objectPrefab)
    {
        ObjectPool pool = GetPoolByName(poolName);
        GameObject goToPool = Instantiate(objectPrefab);
        goToPool.transform.parent = pool.parentForObjects;
        pool.ObjectsPool.Add(goToPool);
        goToPool.SetActive(false);
        return goToPool;
    }

    public ObjectPool GetPoolByName(string poolNameToGet)
    {
        for (int i = 0; i < objectPoolList.Count; i++)
        {
            if (objectPoolList[i].poolName.Contains(poolNameToGet))
                return objectPoolList[i];
        }
        return null;
    }

    public GameObject GetObjectFromPool(ObjectPool pool)
    {
        GameObject go = null;
        for (int i = 0; i < pool.ObjectsPool.Count; i++)
        {
            if (!pool.ObjectsPool[i].activeInHierarchy)
                go = pool.ObjectsPool[i];
        }
        if (go == null)
            go = AddObjectToPool(pool);
        return go;
    }
    
}
